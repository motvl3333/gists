import bpy
import os
import random

# get object context
obj = bpy.context.active_object
mesh = obj.to_mesh(bpy.context.scene, False, 'PREVIEW')

# FIXME: use relative file path name
with open('C:\\Dev\\tests\\blender\\s3d_test\\a.s3d', 'wb') as f:

    # header section
    ID = b'\x00\x00\x00\x00'
    nvert = (len(mesh.vertices)).to_bytes(2, byteorder='little', signed=False)
    npoly = (len(mesh.polygons)).to_bytes(2, byteorder='little', signed=False)

    # write the ID block
    f.write(ID)
    
    # write the unused block
    f.write(b'\x00\x00\x00\x00')

    # write the nvert and npoly block
    f.write(nvert + npoly)
    
    # write the VERTEX section
    for v in mesh.vertices:
        for dim in range(3):
            f.write( (int(v.co[dim])).to_bytes(2, byteorder="little", signed=True) )
    
    # finally, write the POLYGON section
    for face in mesh.tessfaces:
        vert = len(face.vertices)
        
        if vert == 3:
            p_type = b'\x00'
        elif vert == 4:
            p_type = b'\x01'
        else:
            p_type = b'\255'
        
        f.write(p_type + b'\x00\x00\x00')
        
        for i in range(vert):
            f.write( (face.vertices[i]).to_bytes(2, byteorder="little", signed=False) )
        
        # write unused 4th index
        if vert == 3:
            f.write(b'\x00\x00')
        
        # FIXME: write the color values from the mesh itself
        f.write((random.randint(0, 255)).to_bytes(1, byteorder="little", signed=False))
        f.write((random.randint(0, 255)).to_bytes(1, byteorder="little", signed=False))
        f.write((random.randint(0, 255)).to_bytes(1, byteorder="little", signed=False))
        
        f.write(b'\x00') # unused byte
